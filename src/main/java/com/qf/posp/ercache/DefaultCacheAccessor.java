package com.qf.posp.ercache;

import com.qf.posp.ercache.autoconfigure.CacheProperties;
import com.qf.posp.ercache.core.CacheLevel;
import com.qf.posp.ercache.core.CacheResult;
import com.qf.posp.ercache.core.NullObject;
import com.qf.posp.ercache.exception.ErCacheException;
import com.qf.posp.ercache.exception.LoadSourceDataException;
import com.qf.posp.ercache.support.ClusterCacheSyncHandler;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

/**
 * 名称: DefaultCacheAccessor.java <br>
 * 描述:缓存服务访问器<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 11:46<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 11:46
 */
@RequiredArgsConstructor
@Slf4j
public class DefaultCacheAccessor implements ICacheAccessor {

    private final CacheProviderHelp cacheProviderHelp;

    private final ClusterCacheSyncHandler clusterCacheSyncHandler;

    private final CacheProperties cacheProperties;

    private static final Map<String, Object> CACHE_LOCK_KEY_MAP = new ConcurrentHashMap<>();

    @Override
    public void set(String region, String key, Object value) {
        set(region, key, value, -1L);
    }

    @Override
    public void set(String region, String key, Object value, long timeToLiveInSeconds) {
        try {
            if (Objects.isNull(value)) {
                return;
            }
            long ttl = timeToLiveInSeconds > 0 ? timeToLiveInSeconds : cacheProviderHelp.getL1Cache(region).ttl();
            cacheProviderHelp.getL1Cache(region).put(key, value);
            cacheProviderHelp.getL2Cache(region).put(key, value, ttl);
        } catch (Exception err) {
            log.error(err.getMessage(), err);
        } finally {
            //清除其他节点一级缓存
            clusterCacheSyncHandler.sendEvictCommand(region, false, key);
        }
    }

    @Override
    public void mset(String region, Map<String, Object> items) {
        mset(region, items, -1L);
    }

    @Override
    public void mset(String region, Map<String, Object> items, long timeToLiveInSeconds) {
        try {
            if (CollectionUtils.isEmpty(items)) {
                return;
            }
            cacheProviderHelp.getL1Cache(region).mput(items);
            long ttl = timeToLiveInSeconds > 0 ? timeToLiveInSeconds : cacheProviderHelp.getL1Cache(region).ttl();
            cacheProviderHelp.getL2Cache(region).mput(items, ttl);
        } catch (Exception err) {
            log.error(err.getMessage(), err);
        } finally {
            //清除其他节点一级缓存
            clusterCacheSyncHandler.sendEvictCommand(region, false,
                items.keySet().stream().toArray(String[]::new));
        }
    }

    @Override
    public CacheResult get(String region, String key) {
        return get(region, key, k -> null);
    }

    @Override
    public Map<String, CacheResult> mget(String region, Collection<String> keys) {
        if (!CollectionUtils.isEmpty(keys)) {
            log.debug("尝试从一级缓存中读取 region:{},key size:{}", region, keys.size());
            Map<String, Object> objectMap = cacheProviderHelp.getL1Cache(region).mget(keys);

            List<String> l2Keys = keys.stream()
                .filter(k -> !objectMap.containsKey(k) || Objects.isNull(objectMap.get(k)))
                .collect(Collectors.toList());
            //转换结果集
            Map<String, CacheResult> result = objectMap
                .entrySet()
                .stream()
                .filter(entrty -> !Objects.isNull(entrty.getValue()))
                .collect(Collectors.toMap(entrty -> entrty.getKey(), entrty ->
                    CacheResult.builder()
                        .value(entrty.getValue() instanceof NullObject ? null : entrty.getValue())
                        .isNone(entrty.getValue() instanceof NullObject ? true : false)
                        .cacheLevel(CacheLevel.L1)
                        .key(entrty.getKey())
                        .region(region)
                        .build()
                ));

            if (!CollectionUtils.isEmpty(l2Keys)) {
                log.debug("尝试从二级缓存中读取 region:{},key size:{}", region, l2Keys.size());
                Map<String, Object> objectMapFromL2 = cacheProviderHelp.getL2Cache(region).mget(l2Keys);
                if (!CollectionUtils.isEmpty(objectMapFromL2)) {
                    objectMapFromL2.forEach((k, v) -> {
                        result.put(k, CacheResult.builder()
                            .value(v instanceof NullObject ? null : v)
                            .isNone(v instanceof NullObject ? true : false)
                            .cacheLevel(CacheLevel.L2)
                            .key(k)
                            .region(region)
                            .build());
                        if (!Objects.isNull(v)) {
                            //设置到一级缓存中
                            cacheProviderHelp.getL1Cache(region).put(k, v);
                        }
                    });

                }
            }
            return result;
        }
        return null;
    }

    @Override
    public CacheResult get(String region, String key, Function<String, Object> loader) {
        log.debug("尝试从一级缓存中获取 region:{},key:{}", region, key);
        Object object = cacheProviderHelp.getL1Cache(region).get(key);
        if (!Objects.isNull(object)) {
            return CacheResult.builder().region(region)
                .value(object instanceof NullObject ? null : object)
                .key(key)
                .isNone(object instanceof NullObject ? true : false)
                .cacheLevel(CacheLevel.L1)
                .value(object)
                .build();
        }
        log.debug("尝试从二级缓存中获取 region:{},key:{}", region, key);
        String lockKey = new StringBuffer(region).append("@").append(key).toString();
        try {
            synchronized (CACHE_LOCK_KEY_MAP.computeIfAbsent(lockKey, v -> new Object())) {
                log.debug("成功获取到锁 region:{},key:{}", region, key);
                log.debug("再次尝试从一级缓存中读取 region:{},key:{}", region, key);
                object = cacheProviderHelp.getL1Cache(region).get(key);
                if (!Objects.isNull(object)) {
                    return CacheResult.builder().region(region)
                        .key(key)
                        .cacheLevel(CacheLevel.L1)
                        .value(object instanceof NullObject ? null : object)
                        .isNone(object instanceof NullObject ? true : false)
                        .build();
                }
                try {
                    log.debug("尝试从二级缓存读取数据,region:{},key:{}", region, key);
                    object = cacheProviderHelp.getL2Cache(region).get(key);
                } catch (Exception err) {
                    log.error("从二级缓存获取数据失败,region:{},key:{}", region, key, err);
                }
                if (!Objects.isNull(object)) {
                    log.debug("成功从二级缓存中获取数据,准备放置到一级缓存中 region:{},key:{}", region, key);
                    cacheProviderHelp.getL1Cache(region).put(key, object);
                    return CacheResult.builder().region(region)
                        .key(key)
                        .cacheLevel(CacheLevel.L2)
                        .value(object instanceof NullObject ? null : object)
                        .isNone(object instanceof NullObject ? true : false)
                        .build();
                } else {
                    log.debug("尝试从数据源中获取 region:{},key:{}", region, key);
                    Object srcObject;
                    try {
                        srcObject = loader.apply(key);
                    } catch (Exception err) {
                        String message = String.format("从数据源中获取数据失败,region:%s,key:%s", region, key);
                        log.error(message, err);
                        throw new LoadSourceDataException(message, err);
                    }
                    if (!Objects.isNull(srcObject)) {
                        log.debug("从数据源中获取数据,并设置到一级、二级缓存中 region:{},key:{}", region, key);
                        cacheProviderHelp.getL1Cache(region).put(key, srcObject);
                        long ttl = cacheProviderHelp.getL1Cache(region).ttl();
                        cacheProviderHelp.getL2Cache(region).put(key, srcObject, ttl);
                        return CacheResult.builder().region(region)
                            .key(key)
                            .cacheLevel(CacheLevel.L3)
                            .value(srcObject)
                            .build();
                    } else {
                        //如果数据源中也没有,则放置一个"none"到一级、二级缓存中,并设置二级缓存过期时间为5分钟
                        cacheProviderHelp.getL1Cache(region).put(key, new NullObject());
                        cacheProviderHelp.getL2Cache(region).put(key, new NullObject(),
                            cacheProperties.getNoneCacheTimeToLiveInSeconds());
                        return CacheResult
                            .builder()
                            .region(region)
                            .cacheLevel(CacheLevel.L3)
                            .isNone(true)
                            .key(key).build();
                    }
                }
            }
        } catch (Exception err) {
            String message = String.format("缓存读取失败,region:%s,key:%s", region, key);
            log.error(message, err);
            throw new ErCacheException(message, err);
        } finally {
            CACHE_LOCK_KEY_MAP.remove(lockKey);
            log.debug("释放锁资源 region:{},key:{}", region, key);
        }
    }

    @Override
    public Map<String, CacheResult> mget(String region, Collection<String> keys, Function<String, Object> loader) {
        if (!CollectionUtils.isEmpty(keys)) {
            Map<String, CacheResult> result = mget(region, keys);
            if (!CollectionUtils.isEmpty(result)) {
                result.entrySet()
                    .stream()
                    .filter(entrty -> entrty.getValue() == null)
                    .forEach(item -> {
                        String lockKey = new StringBuffer(region).append("@").append(item.getKey()).toString();
                        synchronized (CACHE_LOCK_KEY_MAP.computeIfAbsent(lockKey, v -> new Object())) {
                            CacheResult cacheResult = get(region, item.getKey());
                            if (Objects.isNull(cacheResult) || Objects.isNull(cacheResult.getValue())) {
                                try {
                                    Object object = loader.apply(item.getKey());
                                    if (!Objects.isNull(object)) {
                                        result.put(item.getKey(), CacheResult.builder()
                                            .value(object)
                                            .cacheLevel(CacheLevel.L3)
                                            .key(item.getKey())
                                            .region(region)
                                            .build());
                                        //设置到一级、二级缓存中
                                        long ttl = cacheProviderHelp.getL1Cache(region).ttl();
                                        set(region, item.getKey(), object, ttl);
                                    } else {
                                        object = new NullObject();
                                        result.put(item.getKey(), CacheResult.builder()
                                            .cacheLevel(CacheLevel.L3)
                                            .isNone(true)
                                            .key(item.getKey())
                                            .region(region)
                                            .build());
                                        //设置到一级、二级缓存中
                                        long ttl = cacheProviderHelp.getL1Cache(region).ttl();
                                        set(region, item.getKey(), object, ttl);
                                    }

                                } finally {
                                    CACHE_LOCK_KEY_MAP.remove(lockKey);
                                }

                            } else {
                                result.put(item.getKey(), cacheResult);
                            }
                        }
                    });
            }
            return result;
        }
        return null;
    }

    @Override
    public void evict(String region, String... keys) {
        try {
            //清除本地一级缓存
            cacheProviderHelp.getL1Cache(region).evict(keys);
            //清除远程二级缓存
            cacheProviderHelp.getL2Cache(region).evict(keys);
        } catch (Exception err) {
            log.error("清除缓存失败", err);
        } finally {
            //清除所有节点的一级缓存
            clusterCacheSyncHandler.sendEvictCommand(region, keys);
        }
    }

    @Override
    public void clear(String region) {
        try {
            //清除本地一级缓存
            cacheProviderHelp.getL1Cache(region).clear();
            //清除远程二级缓存
            cacheProviderHelp.getL2Cache(region).clear();
        } catch (Exception err) {
            log.error("清除缓存失败", err);
        } finally {
            //清除所有节点的一级缓存
            clusterCacheSyncHandler.sendClearCommand(region);
        }
    }
}
