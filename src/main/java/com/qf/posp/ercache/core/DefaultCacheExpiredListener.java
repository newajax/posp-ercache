package com.qf.posp.ercache.core;

import com.qf.posp.ercache.CacheProviderHelp;
import com.qf.posp.ercache.support.ClusterCacheSyncHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 名称: DefaultCacheExpiredListener.java <br>
 * 描述:一级缓存失效监听器<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 11:37<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 11:37
 */
@Slf4j
@RequiredArgsConstructor
public class DefaultCacheExpiredListener implements CacheExpiredListener {

    private final CacheProviderHelp cacheProviderHelp;

    private final ClusterCacheSyncHandler clusterCacheSyncHandler;


    @Override
    public void execute(String region, String key) {
        log.debug("监听到一级缓存已过期 region:{},key:{}", region, key);
        //1.清除二级缓存中的数据
        cacheProviderHelp.getL2Cache(region).evict(key);
        //2.清除所有节点的一级缓存
        clusterCacheSyncHandler.sendEvictCommand(region, key);
    }
}
