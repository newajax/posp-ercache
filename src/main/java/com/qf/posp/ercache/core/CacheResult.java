package com.qf.posp.ercache.core;

import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 名称: CacheResult.java <br>
 * 描述:<br>
 * 类型: JAVA<br> 最近修改时间:2018/8/3 14:39<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 14:39
 */
@Builder
@Setter
@Getter
@ToString
public class CacheResult implements Serializable {

    private static final long serialVersionUID = -352135724147496300L;

    private String region;
    private String key;
    private CacheLevel cacheLevel;
    private boolean isNone;
    private Object value;

}
