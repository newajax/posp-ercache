package com.qf.posp.ercache.core;

import java.io.Serializable;

/**
 * 名称: NullObject.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/6 16:03<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/6 16:03
 */
public class NullObject implements Serializable {
    private static final long serialVersionUID = 6555704195985862848L;
    private boolean isNone = true;
}
