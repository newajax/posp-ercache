package com.qf.posp.ercache.core;

import java.util.Map;

/**
 * 名称: L2Cache.java <br>
 * 描述:二级缓存接口<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:22<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:22
 */
public interface L2Cache extends Cache{

    /**
     * 设置缓存,可以设置缓存过期时间
     * @param key
     * @param value
     * @param timeToLiveInSeconds
     */
    void put(String key, Object value,long timeToLiveInSeconds);

    /**
     * 批量设置缓存
     * @param items
     */
    void mput(Map<String,Object> items,long timeToLiveInSeconds);

}
