package com.qf.posp.ercache.core;

/**
 * 名称: CacheExpiredListener.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:36<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:36
 */
public interface CacheExpiredListener {


    void execute(String region, String key);

}
