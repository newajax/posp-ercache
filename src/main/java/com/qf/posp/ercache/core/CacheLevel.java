package com.qf.posp.ercache.core;

import lombok.Getter;

/**
 * 名称: CacheLevel.java <br>
 * 描述:缓存级别<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:49<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:49
 */
public enum CacheLevel {

    L1(1),

    L2(2),

    L3(3);

    @Getter
    private int level;

    CacheLevel(int level){
        this.level = level;
    }
}
