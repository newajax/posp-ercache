package com.qf.posp.ercache.core;

import com.qf.posp.ercache.autoconfigure.CacheProperties;

/**
 * 名称: CacheProvider.java <br>
 * 描述:缓存服务提供者<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:46<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:46
 */
public interface CacheProvider {

    String name();

    CacheLevel level();

    Cache buildCache(String region, CacheExpiredListener listener);

    void start(CacheProperties props);

    void stop();

}
