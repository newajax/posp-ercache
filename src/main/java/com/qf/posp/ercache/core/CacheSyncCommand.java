package com.qf.posp.ercache.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Administrator on 2018/8/4.
 */
@Setter
@Getter
@NoArgsConstructor
public class CacheSyncCommand {

    private String instanceId;
    private String commandKey;
    private String region;
    private boolean isClearCurrentNodeL1Cache = true;
    private String[] keys;

    public CacheSyncCommand(String instanceId,String commandKey,String region,boolean isClearCurrentNodeL1Cache,String ...keys){
        this.instanceId = instanceId;
        this.commandKey = commandKey;
        this.region = region;
        this.isClearCurrentNodeL1Cache = isClearCurrentNodeL1Cache;
        this.keys = keys;
    }
    public CacheSyncCommand(String instanceId,String commandKey,String region,boolean isClearCurrentNodeL1Cache){
        this.instanceId = instanceId;
        this.commandKey = commandKey;
        this.region = region;
        this.isClearCurrentNodeL1Cache = isClearCurrentNodeL1Cache;
    }
}
