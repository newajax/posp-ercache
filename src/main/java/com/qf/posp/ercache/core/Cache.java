package com.qf.posp.ercache.core;

import java.util.Collection;
import java.util.Map;

/**
 * 名称: Cache.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:21<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:21
 */
public interface Cache {

    /**
     * 读取缓存
     * @param key
     * @return
     */
    Object get(String key);

    /**
     * 批量读取缓存
     * @param keys
     * @return
     */
    Map<String,Object> mget(Collection<String> keys);

    /**
     * 判断缓存是否存在
     * @param key
     * @return
     */
    boolean exists(String key);

    /**
     * 获取所有缓存的key
     * @return
     */
    Collection<String> keys();

    /**
     * 清除缓存
     * @param keys
     */
    void evict(String...keys);

    /**
     * 清楚缓存
     */
    void clear();


}
