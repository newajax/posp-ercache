package com.qf.posp.ercache.core;

import java.util.Map;

/**
 * 名称: L1Cache.java <br>
 * 描述:一级缓存接口<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:22<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:22
 */
public interface L1Cache extends Cache{

    /**
     * 设置缓存
     * @param key
     * @param value
     */
    void put(String key, Object value);


    /**
     * 返回该缓存区域的 TTL 设置（单位：秒）
     * @return true if cache support ttl setting
     */
    long ttl();

    /**
     * 返回该缓存区域中，内存存储对象的最大数量
     * @return cache size in memory
     */
    long size();


    /**
     * 批量设置缓存
     * @param items
     */
    void mput(Map<String,Object> items);


}
