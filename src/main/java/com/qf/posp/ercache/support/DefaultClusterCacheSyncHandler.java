package com.qf.posp.ercache.support;

import com.qf.posp.ercache.CacheProviderHelp;
import com.qf.posp.ercache.core.CacheSyncCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by Administrator on 2018/8/4.
 */
@RequiredArgsConstructor
public class DefaultClusterCacheSyncHandler implements ClusterCacheSyncHandler {

    private final RedisTemplate<String, String> redisTemplate;
    private final String redisPubSubChannelName;

    @Override
    public void sendEvictCommand(String region, String... keys) {
        sendEvictCommand(region, true, keys);
    }

    @Override
    public void sendClearCommand(String region) {
        sendClearCommand(region, true);
    }

    @Override
    public void sendEvictCommand(String region, boolean isClearCurrentNodeLocalCache, String... keys) {
        redisTemplate.convertAndSend(redisPubSubChannelName, new CacheSyncCommand(
            CacheProviderHelp.getInstanceId(), EVICT, region, isClearCurrentNodeLocalCache,
            keys));
    }

    @Override
    public void sendClearCommand(String region, boolean isClearCurrentNodeLocalCache) {
        redisTemplate.convertAndSend(redisPubSubChannelName, new CacheSyncCommand(
            CacheProviderHelp.getInstanceId(), CLEAR, region, isClearCurrentNodeLocalCache));
    }
}
