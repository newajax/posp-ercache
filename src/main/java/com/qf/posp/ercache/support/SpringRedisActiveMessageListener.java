package com.qf.posp.ercache.support;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.posp.ercache.CacheProviderHelp;
import com.qf.posp.ercache.core.CacheSyncCommand;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * 名称: SpringRedisActiveMessageListener.java <br>
 * 描述:监听二级缓存失败时,删除本地一级缓存<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 10:26<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 10:26
 */
@RequiredArgsConstructor
@Slf4j
public class SpringRedisActiveMessageListener implements MessageListener {

    private final CacheProviderHelp cacheProviderHelp;

    private static final Jackson2JsonRedisSerializer jackson2JsonRedisSerializer;

    static {
        jackson2JsonRedisSerializer =
            new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String commandJosnString = message.toString();
        log.debug("监听到删除缓存命令:{}", commandJosnString);
        CacheSyncCommand cacheSyncCommand = null;
        try {
            cacheSyncCommand = (CacheSyncCommand) jackson2JsonRedisSerializer.
                deserialize(commandJosnString.getBytes("UTF-8"));
        } catch (SerializationException | UnsupportedEncodingException err) {
            log.error(err.getMessage(), err);
        }
        if (!Objects.isNull(cacheSyncCommand)) {
            if (!cacheSyncCommand.isClearCurrentNodeL1Cache()) {
                // 如果是当前节点,则什么也不做
                if (CacheProviderHelp.getInstanceId().equals(cacheSyncCommand.getInstanceId())) {
                    log.debug("instanceId:{},为当前节点,不做删除操作,region:{}",
                        cacheSyncCommand.getInstanceId(), cacheSyncCommand.getRegion());
                    return;
                }
            }
            if (ClusterCacheSyncHandler.EVICT.equals(cacheSyncCommand.getCommandKey())) {
                cacheProviderHelp.getL1Cache(
                    cacheSyncCommand.getRegion()).evict(cacheSyncCommand.getKeys());
            }
            if (ClusterCacheSyncHandler.CLEAR.equals(cacheSyncCommand.getCommandKey())) {
                cacheProviderHelp.getL1Cache(cacheSyncCommand.getRegion()).clear();
            }
        }
    }
}
