package com.qf.posp.ercache.support;

/**
 * 名称: ClusterPolicy.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 10:17<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 10:17
 */
public interface ClusterCacheSyncHandler {

    static final String EVICT = "evict";

    static final String CLEAR = "clear";

    void sendEvictCommand(String region, String... keys);

    void sendClearCommand(String region);

    void sendEvictCommand(String region, boolean isClearCurrentNodeLocalCache, String... keys);

    void sendClearCommand(String region, boolean isClearCurrentNodeLocalCache);



}
