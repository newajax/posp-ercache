package com.qf.posp.ercache.autoconfigure;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Import;

/**
 * 名称: EnableErCache.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 14:02<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 14:02
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AutoCacheConfiguration.class)
@Documented
public @interface EnableErCache {

}
