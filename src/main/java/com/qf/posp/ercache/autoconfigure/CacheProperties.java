package com.qf.posp.ercache.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 名称: ErCacheProperties.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:30<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:30
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "com.qf.posp.ercache")
public class CacheProperties {

    private String ehcacheConfigXmlFileName = "/ehcache3.xml";

    private String redisNamespace = "posp:ercache";

    private String redisPubSubChannelName = "ercache-channel";

    /**
     * 默认缓存过期时间
     */
    private Long noneCacheTimeToLiveInSeconds = 5 * 60L;
}
