package com.qf.posp.ercache.autoconfigure;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.qf.posp.ercache.CacheProviderHelp;
import com.qf.posp.ercache.DefaultCacheAccessor;
import com.qf.posp.ercache.ICacheAccessor;
import com.qf.posp.ercache.core.CacheExpiredListener;
import com.qf.posp.ercache.core.DefaultCacheExpiredListener;
import com.qf.posp.ercache.support.ClusterCacheSyncHandler;
import com.qf.posp.ercache.support.DefaultClusterCacheSyncHandler;
import com.qf.posp.ercache.support.SpringRedisActiveMessageListener;
import com.qf.posp.ercache.util.ApplicationContextKit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 名称: CacheConfiguration.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:29<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:29
 */
@Configuration
@ConditionalOnClass( {RedisTemplate.class})
@EnableConfigurationProperties( {CacheProperties.class})
public class AutoCacheConfiguration {

    private CacheProperties cacheProperties;

    @Value("${spring.profiles.active}")
    private String active;

    public AutoCacheConfiguration(CacheProperties cacheProperties) {
        this.cacheProperties = cacheProperties;
    }

    /**
     * Spring 容器工具类
     */
    @Bean
    public ApplicationContextKit applicationContextKit() {
        return new ApplicationContextKit();
    }

    /**
     * redisTemplate 用户操作redis
     */
    @Bean(name = "erCacheRedisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer =
            new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        //设置value序列化方式
        template.setValueSerializer(jackson2JsonRedisSerializer);

        //设置value序列化方式
        //template.setValueSerializer(new FastJsonRedisSerializer(Serializable.class));

        //设置key的序列化方式
        template.setKeySerializer(new StringRedisSerializer());
        template.afterPropertiesSet();
        return template;
    }

    /**
     * 集群缓存同步处理器
     */
    @Bean
    public ClusterCacheSyncHandler clusterCacheSyncHandler(RedisTemplate erCacheRedisTemplate) {
        return new DefaultClusterCacheSyncHandler(erCacheRedisTemplate, cacheProperties.getRedisPubSubChannelName());
    }

    /**
     * 一级缓存失效监听器
     */
    @Bean
    public CacheExpiredListener defaultCacheExpiredListener(@Lazy CacheProviderHelp cacheProviderHelp,
        ClusterCacheSyncHandler clusterCacheSyncHandler) {
        return new DefaultCacheExpiredListener(cacheProviderHelp,clusterCacheSyncHandler);
    }

    /**
     * 缓存Provider 帮助类
     */
    @Bean(initMethod = "init", destroyMethod = "stop")
    @DependsOn({"applicationContextKit"})
    public CacheProviderHelp cacheProviderHelp(@Lazy CacheExpiredListener defaultCacheExpiredListener) {
        if (!Strings.isNullOrEmpty(active)) {
            cacheProperties.setRedisNamespace(active + ":" + cacheProperties.getRedisNamespace());
        }
        return new CacheProviderHelp(cacheProperties, defaultCacheExpiredListener);
    }

    /**
     * 二级缓存清除监听器
     */
    @Bean
    public MessageListener springRedisActiveMessageListener(CacheProviderHelp cacheProviderHelp) {
        return new SpringRedisActiveMessageListener(cacheProviderHelp);
    }


    /**
     * redis MessageListener 容器
     */
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,
        MessageListener springRedisActiveMessageListener) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        container.addMessageListener(springRedisActiveMessageListener,
            new ChannelTopic(cacheProperties.getRedisPubSubChannelName()));
        return container;
    }

    /**
     * 缓存操作服务
     */
    @Bean
    public ICacheAccessor cacheAccessor(CacheProviderHelp cacheProviderHelp,
        ClusterCacheSyncHandler clusterCacheSyncHandler) {
        return new DefaultCacheAccessor(cacheProviderHelp, clusterCacheSyncHandler, cacheProperties);
    }

}
