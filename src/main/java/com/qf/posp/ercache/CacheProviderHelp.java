package com.qf.posp.ercache;

import com.qf.posp.ercache.autoconfigure.CacheProperties;
import com.qf.posp.ercache.core.CacheExpiredListener;
import com.qf.posp.ercache.core.CacheProvider;
import com.qf.posp.ercache.core.L1Cache;
import com.qf.posp.ercache.core.L2Cache;
import com.qf.posp.ercache.ehcache.EhCache3Provider;
import com.qf.posp.ercache.redis.SpringRedisProvider;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 名称: CacheProviderHelp.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 11:11<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 11:11
 */
@Slf4j
@RequiredArgsConstructor
public class CacheProviderHelp {

    private final CacheProperties cacheProperties;

    private final CacheExpiredListener listener;

    private CacheProvider cacheProviderL1;

    private CacheProvider cacheProviderL2;

    private static final String INSTANCE_ID;

    static {
        INSTANCE_ID = UUID.randomUUID().toString();
    }

    public void init(){
        cacheProviderL1 = new EhCache3Provider();
        cacheProviderL1.start(cacheProperties);
        cacheProviderL2 = new SpringRedisProvider();
        cacheProviderL2.start(cacheProperties);

    }
    public void stop(){
        cacheProviderL1.stop();
        cacheProviderL2.stop();
    }
    public L1Cache getL1Cache(String region){
        return (L1Cache)cacheProviderL1.buildCache(region,listener);
    }

    public L2Cache getL2Cache(String region){
        return (L2Cache) cacheProviderL2.buildCache(region,listener);
    }

    public static String getInstanceId(){
        return INSTANCE_ID;
    }


}
