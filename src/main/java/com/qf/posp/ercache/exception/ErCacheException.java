package com.qf.posp.ercache.exception;

/**
 * 名称: ErCacheException.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/4 19:19<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/4 19:19
 */
public class ErCacheException extends RuntimeException{


    public ErCacheException(){
        super();
    }


    public ErCacheException(String message){
        super(message);
    }

    public ErCacheException(String message, Throwable cause){
        super(message,cause);
    }
}
