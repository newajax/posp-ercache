package com.qf.posp.ercache.exception;

/**
 * 名称: LoadSourceDataException.java <br>
 * 描述:获取源数据异常<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/4 19:19<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/4 19:19
 */
public class LoadSourceDataException extends RuntimeException{
    public LoadSourceDataException(){
        super();
    }
    public LoadSourceDataException(String message){
        super(message);
    }
    public LoadSourceDataException(String message, Throwable cause){
        super(message,cause);
    }
}
