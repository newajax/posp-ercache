package com.qf.posp.ercache.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 名称: ApplicationContextKit.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 10:05<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 10:05
 */
public class ApplicationContextKit implements ApplicationContextAware {


    private static volatile ApplicationContext APPLICATION_CONTEXT;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        init(applicationContext);
    }

    public static ApplicationContext getApplicationContext() {
        return APPLICATION_CONTEXT;
    }

    public static void init(ApplicationContext applicationContext) {
        APPLICATION_CONTEXT = applicationContext;
    }

}
