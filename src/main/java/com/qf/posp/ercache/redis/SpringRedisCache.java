package com.qf.posp.ercache.redis;

import com.qf.posp.ercache.core.L2Cache;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

/**
 * 名称: SpringRedisCache.java <br>
 * 描述: Spring实现二级缓存<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 18:35<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 18:35
 */
public class SpringRedisCache implements L2Cache {

    private String namespace;

    private String region;

    private RedisTemplate<String, Object> redisTemplate;

    public SpringRedisCache(String namespace, String region, RedisTemplate<String, Object> redisTemplate) {
        this.namespace = namespace;
        this.region = region;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Object get(String key) {
        return redisTemplate.opsForValue().get(buildCacheKey(key));
    }

    @Override
    public Map<String, Object> mget(Collection<String> keys) {
        if (!CollectionUtils.isEmpty(keys)) {
            Collection<String> newKeys = keys.stream().map(k -> buildCacheKey(k)).collect(Collectors.toSet());
            List<Object> list = redisTemplate.opsForValue().multiGet(newKeys);
            if (!CollectionUtils.isEmpty(list)) {
                Map<String, Object> result = new HashMap<>(list.size());
                int i = 0;
                for (String key : keys) {
                    result.put(key, list.get(i++));
                }
                return result;
            }

        }
        return null;
    }

    @Override
    public void mput(Map<String, Object> items, long timeToLiveInSeconds) {
        if (!CollectionUtils.isEmpty(items)) {
            items = items
                .entrySet()
                .stream()
                .collect(Collectors.toMap(item -> buildCacheKey(item.getKey()), item -> item.getValue()));
            redisTemplate.opsForValue().multiSet(items);
            //设置过期时间
            items.keySet().forEach(key -> redisTemplate.expire(key,timeToLiveInSeconds,TimeUnit.SECONDS));
        }
    }

    @Override
    public boolean exists(String key) {
        return redisTemplate.hasKey(buildCacheKey(key));
    }

    @Override
    public Collection<String> keys() {
        return redisTemplate.keys(buildCacheKey("*"));
    }

    @Override
    public void evict(String... keys) {
        redisTemplate.delete(buildCacheKeys(keys));
    }

    @Override
    public void clear() {
        redisTemplate.delete(keys());
    }

    @Override
    public void put(String key, Object value, long timeToLiveInSeconds) {
        if (!Objects.isNull(value)) {
            redisTemplate.opsForValue().
                set(buildCacheKey(key), value, timeToLiveInSeconds,
                    TimeUnit.SECONDS);
        }
    }

    private String buildCacheKey(String key) {
        return new StringBuilder(namespace).append(":").append(region).append(":").append(key).toString();
    }

    private Set<String> buildCacheKeys(String... keys) {
        return Arrays.asList(keys)
            .stream()
            .map(k -> new StringBuilder(namespace).append(":").append(region).append(":").append(k).toString())
            .collect(Collectors.toSet());
    }
}
