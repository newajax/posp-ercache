package com.qf.posp.ercache.redis;

import com.qf.posp.ercache.autoconfigure.CacheProperties;
import com.qf.posp.ercache.core.Cache;
import com.qf.posp.ercache.core.CacheExpiredListener;
import com.qf.posp.ercache.core.CacheLevel;
import com.qf.posp.ercache.core.CacheProvider;
import com.qf.posp.ercache.util.ApplicationContextKit;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 名称: SpringRedisProvider.java <br>
 * 描述:Spring 实现二级缓存CacheProvider<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 9:52<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 9:52
 */
@Slf4j
public class SpringRedisProvider implements CacheProvider {

    private RedisTemplate<String, Object> redisTemplate;

    private String namespace;

    protected ConcurrentHashMap<String, Cache> caches = new ConcurrentHashMap<>();

    @Override
    public String name() {
        return "redis";
    }

    @Override
    public CacheLevel level() {
        return CacheLevel.L2;
    }

    @Override
    public Cache buildCache(String region, CacheExpiredListener listener) {
        Cache cache = caches.get(region);
        if (Objects.isNull(cache)) {
            synchronized (SpringRedisProvider.class) {
                cache = caches.get(region);
                if (Objects.isNull(cache)) {
                    cache = new SpringRedisCache(this.namespace, region, redisTemplate);
                    caches.put(region, cache);
                }

            }
        }
        return cache;
    }


    @Override
    public void start(CacheProperties props) {
        this.namespace = props.getRedisNamespace();
        redisTemplate = ApplicationContextKit.getApplicationContext()
            .getBean("erCacheRedisTemplate", RedisTemplate.class);
    }

    @Override
    public void stop() {

    }
}
