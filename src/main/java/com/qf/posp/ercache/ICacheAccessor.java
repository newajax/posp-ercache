package com.qf.posp.ercache;

import com.qf.posp.ercache.core.CacheResult;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

/**
 * 名称: IERCacheService.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 11:39<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 11:39
 */
public interface ICacheAccessor {

    /**
     * 设置缓存
     * 默认的过期时间为L1的过期时间
     * @param region
     * @param key
     * @param value
     */
    void set(String region,String key,Object value);

    /**
     * 设置缓存
     * @param region
     * @param key
     * @param value
     * @param timeToLiveInSeconds
     */
    void set(String region,String key,Object value,long timeToLiveInSeconds);

    /**
     * 批量设置缓存
     * 每一个 item的过期时间为L1的过期时间
     * @param region
     * @param items
     */
    void mset(String region,Map<String,Object> items);

    /**
     * 批量设置缓存
     * @param region
     * @param items
     * @param timeToLiveInSeconds
     */
    void mset(String region,Map<String,Object> items,long timeToLiveInSeconds);

    /**
     * 获取缓存
     * @param region
     * @param key
     * @return
     */
    CacheResult get(String region,String key);

    /**
     * 批量获取缓存
     * @param region
     * @param keys
     * @return
     */
    Map<String,CacheResult> mget(String region,Collection<String> keys);

    /**
     * 含数据加载器的获取缓存
     * @param region
     * @param key
     * @param loader
     * @return
     */
    CacheResult get(String region,String key,Function<String,Object> loader);

    /**
     * 含数据加载器的批量获取缓存
     * @param region
     * @param keys
     * @param loader
     * @return
     */
    Map<String,CacheResult> mget(String region,Collection<String> keys,Function<String,Object> loader);

    /**
     * 清除单个缓存
     * @param region
     * @param keys
     */
    void evict(String region,String... keys);

    /**
     * 清除区域缓存
     * @param region
     */
    void clear(String region);
}
