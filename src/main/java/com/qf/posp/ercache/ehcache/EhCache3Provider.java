package com.qf.posp.ercache.ehcache;

import com.qf.posp.ercache.autoconfigure.CacheProperties;
import com.qf.posp.ercache.core.Cache;
import com.qf.posp.ercache.core.CacheExpiredListener;
import com.qf.posp.ercache.core.CacheLevel;
import com.qf.posp.ercache.core.CacheProvider;
import java.io.Serializable;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;

/**
 * 名称: EhCache3Provider.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:56<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:56
 */
@Slf4j
public class EhCache3Provider implements CacheProvider {

    /**
     * 缓存默认模板
     */
    private final static String DEFAULT_TPL = "default";

    private CacheManager manager;

    private ConcurrentHashMap<String, EhCache3> caches = new ConcurrentHashMap<>();


    @Override
    public String name() {
        return "ehcache3";
    }

    @Override
    public CacheLevel level() {
        return CacheLevel.L1;
    }

    @Override
    public Cache buildCache(String region, CacheExpiredListener listener) {
        EhCache3 ehCache3 = caches.get(region);
        if (Objects.isNull(ehCache3)) {
            synchronized (EhCache3Provider.class) {
                ehCache3 = caches.get(region);
                if (Objects.isNull(ehCache3)) {
                    //自动搜索ehcache3.xml配置文件配置的cache
                    org.ehcache.Cache cache = manager.getCache(region, String.class, Serializable.class);
                    if (Objects.isNull(cache)) {
                        CacheConfiguration defaultCacheConfig = manager.getRuntimeConfiguration()
                            .getCacheConfigurations().get(DEFAULT_TPL);
                        CacheConfiguration<String, Serializable> cacheCfg =
                            CacheConfigurationBuilder.newCacheConfigurationBuilder(defaultCacheConfig)
                                .build();
                        cache = manager.createCache(region, cacheCfg);
                        log.info("Could not find configuration {}; using defaults.", region);
                    }
                    ehCache3 = new EhCache3(region, cache, listener);
                    caches.put(region, ehCache3);
                }
            }
        }
        return ehCache3;
    }

    @Override
    public void start(CacheProperties props) {
        log.info("----start ehcache manager ------");
        URL url = getClass().getResource(props.getEhcacheConfigXmlFileName());
        Configuration xmlConfig = new XmlConfiguration(url);
        manager = CacheManagerBuilder.newCacheManager(xmlConfig);
        manager.init();

    }

    @Override
    public void stop() {
        log.info("----stop ehcache manager ------");
        if (manager != null) {
            manager.close();
            caches.clear();
            manager = null;
        }
    }
}
