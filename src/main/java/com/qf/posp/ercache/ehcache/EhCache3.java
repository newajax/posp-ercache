package com.qf.posp.ercache.ehcache;

import com.qf.posp.ercache.core.CacheExpiredListener;
import com.qf.posp.ercache.core.L1Cache;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import org.ehcache.config.ResourceType;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.ehcache.event.EventFiring;
import org.ehcache.event.EventOrdering;
import org.ehcache.event.EventType;
import org.ehcache.expiry.Duration;

/**
 * 名称: EhCache3.java <br>
 * 描述: EhCache3 实现一级缓存<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/2 17:34<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/2 17:34
 */

public class EhCache3 implements L1Cache, CacheEventListener {

    private final String name;
    private final org.ehcache.Cache<String, Object> cache;
    private final CacheExpiredListener listener;


    public EhCache3(String name, org.ehcache.Cache<String, Object> cache, CacheExpiredListener listener) {
        this.name = name;
        this.cache = cache;
        this.cache.getRuntimeConfiguration().registerCacheEventListener(this,
            EventOrdering.ORDERED,
            EventFiring.ASYNCHRONOUS,
            EventType.EXPIRED);
        this.listener = listener;

    }

    @Override
    public long ttl() {
        Duration dur = this.cache.getRuntimeConfiguration().getExpiry().getExpiryForCreation(null, null);
        if (dur.isInfinite()) {
            return 0L;
        }
        return dur.getTimeUnit().toSeconds(dur.getLength());
    }

    @Override
    public long size() {
        return this.cache.getRuntimeConfiguration().getResourcePools().getPoolForResource(ResourceType.Core.HEAP)
            .getSize();
    }

    @Override
    public Object get(String key) {
        return this.cache.get(key);
    }

    @Override
    public Map<String, Object> mget(Collection<String> keys) {
        return cache.getAll(keys.stream().collect(Collectors.toSet()));
    }

    @Override
    public void put(String key, Object value) {
        this.cache.put(key, value);
    }

    @Override
    public void mput(Map<String, Object> items) {
        cache.putAll(items);
    }

    @Override
    public boolean exists(String key) {
        return cache.containsKey(key);
    }

    @Override
    public Collection<String> keys() {
        return Collections.emptyList();
    }

    @Override
    public void evict(String... keys) {
        this.cache.removeAll(Arrays.stream(keys).collect(Collectors.toSet()));
    }

    @Override
    public void clear() {
        this.cache.clear();
    }

    @Override
    public void onEvent(CacheEvent event) {
        if (event.getType() == EventType.EXPIRED) {
            this.listener.execute(name, (String) event.getKey());
        }
    }
}
