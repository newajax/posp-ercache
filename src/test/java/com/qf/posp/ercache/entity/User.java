package com.qf.posp.ercache.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 名称: User.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/3 14:23<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/3 14:23
 */
@AllArgsConstructor
@Builder
@Setter
@Getter
@NoArgsConstructor
@ToString
public class User implements Serializable {
    private static final long serialVersionUID = -6203560095152868587L;

    private Long id;

    private String name;

    private Integer age;
}
