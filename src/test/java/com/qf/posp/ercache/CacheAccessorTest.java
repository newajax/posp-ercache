package com.qf.posp.ercache;

import com.google.common.collect.Lists;
import com.qf.posp.ercache.core.CacheLevel;
import com.qf.posp.ercache.core.CacheResult;
import com.qf.posp.ercache.entity.User;
import com.qf.posp.ercache.support.DefaultClusterCacheSyncHandler;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 名称: CacheAccessorTest.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/6 9:52<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/6 9:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppTest.class})
@Slf4j
public class CacheAccessorTest {

    @Autowired
    private ICacheAccessor cacheAccessor;

    @Autowired
    private DefaultClusterCacheSyncHandler clusterCacheSyncHandler;

    @Test
    public void testSet() throws Exception {
        String region = "user";
        String cacheKey = "1";
        User user = User.builder().id(1L).name("er-cache").age(12).build();
        cacheAccessor.set(region, cacheKey, user, 30);
        Thread.sleep(2000L);
        CacheResult cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        User cacheUser = (User) cacheResult.getValue();
        Assert.assertTrue(cacheUser.getName().equals(user.getName()));

        //删除所有节点一级缓存
        clusterCacheSyncHandler.sendEvictCommand(region, true, cacheKey);
        Thread.sleep(2000L);

        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L2);
        cacheUser = (User) cacheResult.getValue();
        Assert.assertTrue(cacheUser.getName().equals(user.getName()));

        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        cacheUser = (User) cacheResult.getValue();
        Assert.assertTrue(cacheUser.getName().equals(user.getName()));


    }

    @Test
    public void testClusterCacheSync() throws Exception {
        String region = "user";
        String cacheKey = "2";
        //删除region下 一级、二级缓存
        cacheAccessor.clear(region);
        /**
         * 这里暂停一会,防止因为redis的 pus/sub延时的情况
         */
        Thread.sleep(2000L);
        User user = User.builder().id(2L).name("zs").age(25).build();
        cacheAccessor.set(region, cacheKey, user, 60);
        CacheResult cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        //删除所有节点一级缓存
        clusterCacheSyncHandler.sendEvictCommand(region, cacheKey);
        /**
         * 这里暂停一会,防止因为redis的 pus/sub延时的情况
         */
        Thread.sleep(2000L);
        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L2);
        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        //删除其他节点一级缓存
        clusterCacheSyncHandler.sendEvictCommand(region, false, cacheKey);
        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
    }

    /**
     * 多线程读取
     */
    @Test
    public void multiThreadRead() throws Exception {
        String region = "user";
        String cacheKey = "3";
        User user = User.builder().id(3L).name("er-cache").age(25).build();
        //清除本地一级缓存和远程二级缓存
        cacheAccessor.evict(region, cacheKey);
        Thread.sleep(2000L);
        long startTime = System.currentTimeMillis();
        int count = 50;
        CountDownLatch countDownLatch = new CountDownLatch(count);
        CountDownLatch readCountDownLatch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            new Thread(() -> {
                try {
                    readCountDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                cacheAccessor.get(region, cacheKey, k -> user);
                countDownLatch.countDown();
            }).start();
            readCountDownLatch.countDown();
        }
        log.info("读取" + count + "次用时" + (System.currentTimeMillis() - startTime) + " ms");
        countDownLatch.await();
    }


    @Test
    public void testMset() throws Exception {
        String region = "user";
        Map<String, Object> map = new HashMap<>(10);
        for (int i = 10; i < 20; i++) {
            map.put(i + "", User.builder()
                .id(Long.parseLong(i + ""))
                .age(i)
                .name("zs" + i)
                .build());
        }
        cacheAccessor.clear(region);
        Thread.sleep(2000L);
        cacheAccessor.mset(region, map);
        CacheResult cacheResult = cacheAccessor.get(region, "10");
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        User user = (User) cacheResult.getValue();
        Assert.assertTrue(user.getName().equals("zs10"));

        //删除所有节点一级缓存
        clusterCacheSyncHandler.sendClearCommand(region, true);
        Thread.sleep(2000L);

        cacheResult = cacheAccessor.get(region, "10");
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L2);
        user = (User) cacheResult.getValue();
        Assert.assertTrue(user.getName().equals("zs10"));

        Map<String, CacheResult> result = cacheAccessor.mget(region, map.keySet());
        Assert.assertTrue(result.size() == map.size());

        for (Map.Entry<String, CacheResult> entry : result.entrySet()) {
            log.info("------>>>>>" + entry.getValue());
        }
    }

    @Test
    public void testCacheExpiredListener() throws Exception {
        String region = "user";
        String cacheKey = "4";
        User user = User.builder().id(4L).name("er-cache-4").age(25).build();
        cacheAccessor.set(region, cacheKey, user);
        CacheResult cacheResult = cacheAccessor.get(region, cacheKey);
        Thread.sleep(15000);

        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);

        cacheResult = cacheAccessor.get(region, cacheKey);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L3
            && cacheResult.isNone());

        Thread.sleep(2000L);
    }



    @Test
    public void testSetList() throws Exception{
        String region = "str";
        String key = "10";
        List<String> value = Lists.newArrayList("10","11","12");
        cacheAccessor.set(region,key,value);
        CacheResult cacheResult = cacheAccessor.get(region,key);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L1);
        clusterCacheSyncHandler.sendEvictCommand(region,key);

        Thread.sleep(2000L);
        cacheResult = cacheAccessor.get(region,key);
        Assert.assertTrue(cacheResult.getCacheLevel() == CacheLevel.L2);
        List<String> cacheValue = (List<String>) cacheResult.getValue();
        Assert.assertTrue(value.size() == cacheValue.size());
    }
}
