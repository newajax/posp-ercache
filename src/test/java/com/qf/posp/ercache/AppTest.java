package com.qf.posp.ercache;

import com.qf.posp.ercache.autoconfigure.EnableErCache;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 名称: AppTest.java <br>
 * 描述:<br>
 * 类型: JAVA<br>
 * 最近修改时间:2018/8/6 10:02<br>
 *
 * @author HaoWu
 * @version [版本号, V1.0]
 * @since 2018/8/6 10:02
 */
@SpringBootApplication
@EnableErCache
public class AppTest {

    public static void main(String[] args) {
        SpringApplication.run(AppTest.class, args);
    }
}
